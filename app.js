const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const productsRoutes = require('./api/controllers/products');
const ordersRoutes = require('./api/controllers/orders');
const usersRoutes = require('./api/controllers/users');

mongoose.connect("mongodb+srv://weslei:" +
    process.env.MONGODB_ATLAS_PASS +
    "@shop-cluster-tvzvb.mongodb.net/test?retryWrites=true",
    {
        useNewUrlParser: true
    })
    .catch(error => {
        console.log(error);
    });

app.use(morgan('dev'));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', '*');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PACTH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
})
app.use('/uploads', express.static('uploads'));
app.use('/users', usersRoutes);
app.use('/products', productsRoutes);
app.use('/orders', ordersRoutes);

app.use((req, res, next) => {
    const error = new Error('not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
})

module.exports = app;