const express = require('express');
const router = express.Router();

const checkAuth = require('../middleware/check-auth');
const OrderController = require('../service/orders');

router.get('/', checkAuth, OrderController.getAllOrders);

router.post('/', OrderController.createOrder);

router.get('/:orderId', OrderController.getOrder);

router.delete('/:orderId', OrderController.deleteOrder);

module.exports = router;