const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const User = require('../models/user');
const bcrypt = require('bcrypt');

exports.login = (req, res, next) => {
    User.find({ email: req.body.email })
        .exec()
        .then(user => {
            if (user.length < 1) {
                return res.status(401).json({
                    message: "Auth failed"
                });
            }
            bcrypt.compare(req.body.password, user[0].password, (error, macth) => {

                if (error) {
                    console.log(error);
                    return res.status(401).json({
                        error: "Auth failed"
                    });
                }
                if (macth) {
                    const token = jwt.sign(
                        {
                            email: user[0].email,
                            userId: user[0]._id
                        },
                        process.env.JWT_KEY,
                        {
                            expiresIn: "1h"
                        });
                    return res.status(200).json({
                        message: 'Auth successful',
                        token: token
                    });
                }
                res.status(401).json({
                    error: "Auth failed"
                });

            })
        })
        .catch(error => {
            res.status(500).json({
                error: error
            })
        });
}

exports.signup = (req, res, next) => {
    
    const email = req.body.email;
    User
        .find({ email: email })
        .exec()
        .then(user => {
            if (user.length >= 1) {
                res.status(422).json({
                    message: 'Mail exists'
                })
            } else {

                bcrypt.hash(req.body.password, 10, (error, hash) => {
                    if (error) {
                        console.log(error);
                        return res.status(500).json({
                            error: error
                        })
                    } else {
                        const user = new User({
                            _id: new mongoose.Types.ObjectId(),
                            email: email,
                            password: hash
                        });
                        user
                            .save()
                            .then(user => {
                                res.status(201).json({
                                    message: 'User created'
                                });
                            })
                            .catch(error => {
                                console.log(error);
                                res.status(500).json({
                                    error: error
                                });
                            });
                    }
                });
            }
        })

}

exports.delete = (req, res, next) => {
    User.remove({ _id: req.params.userId })
        .exec().then(result => {
            res.status(200).json({
                message: 'User deleted'
            })
        }).catch(error => {
            console.log(error);
            res.status(500).json({
                error: error
            })
        });
}