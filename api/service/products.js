const Product = require("../models/product");
const mongoose = require('mongoose');

exports.getAllProducts = (req, res, next) => {
    Product.find()
        .select('name price description _id productImage')
        .exec()
        .then(documents => {
            const response = {
                documents: documents.length,
                products: documents.map(doc => {
                    return {
                        _id: doc._id,
                        name: doc.name,
                        price: doc.price,
                        description: doc.description,
                        productImage: doc.productImage,
                        request: {
                            type: 'GET',
                            url: 'http://localhost:3000/products/' + doc._id
                        }
                    }
                })
            }
            res.status(200).json(response)
        }).catch(error => {
            res.status(500).json({
                error: error
            })
        });
}

exports.createProduct = (req, res, next) => {

    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price,
        description: req.body.description,
        productImage: req.file.path
    });

    product
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'Product was created!',
                product: {
                    _id: result._id,
                    name: result.name,
                    price: result.price,
                    description: result.description,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/products/" + result._id
                    }
                }
            });
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({
                error: error
            });
        });

}

exports.getProduct = (req, res, next) => {
    const id = req.params.productId;

    Product.findById(id)
        .select('name price description _id productImage')
        .exec()
        .then(document => {
            console.log(document);
            if (document) {
                res.status(200).json({
                    _id: document._id,
                    name: document.name,
                    price: document.price,
                    description: document.description,
                    request: {
                        type: 'GET',
                        url: 'http://localhost:3000/products/'
                    }
                });
            } else {
                res.status(500).json({
                    error: "Document doesn't exists"
                })
            }
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({
                message: 'Document not found!',
                error: error
            });
        });

}

exports.updateProduct = (req, res, next) => {
    const id = req.params.productId;
    const updateOps = {};
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }
    Product.findOneAndUpdate({ _id: id }, { $set: updateOps })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Product updated',
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/products/' + result._id
                }
            })
        })
        .catch(error => {
            res.status(500).json({
                error: error
            })
        });

}

exports.deleteProduct = (req, res, next) => {
    const id = req.params.productId;
    Product.findOneAndDelete({ _id: id })
        .exec()
        .then(response => {
            res.status(200).json({
                message: 'Deleted product',
                request: {
                    type: "POST",
                    url: 'http://localhost:3000/products',
                    body: {
                        name: 'String',
                        price: 'Number',
                        description: 'String'
                    }
                }
            })
        })
        .catch(error => {
            res.status(500).json({
                error: error
            })
        });

}