const Order = require('../models/order');
const Product = require('../models/product');
const mongoose = require('mongoose');

exports.getAllOrders = ((req, res, next) => {

    Order
        .find()
        .select('productId quantity _id')
        .populate('productId', 'name')
        .exec()
        .then(orders => {
            res.status(200).json({
                count: orders.length,
                orders: orders.map(order => {
                    return {
                        _id: order._id,
                        quantity: order.quantity,
                        productId: order.productId,
                        request: {
                            type: 'GET',
                            url: 'http://localhost:3000/orders/' + order._id
                        }
                    }
                })
            })
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({
                error: error
            })
        });
});

exports.createOrder = ((req, res, next) => {

    const productId = req.body.productId;
    Product.findById(productId)
        .exec()
        .then(product => {
            if (!product) {
                res.status(404).json({
                    message: 'Product not found'
                })
            }
            const order = new Order({
                _id: mongoose.Types.ObjectId(),
                productId: productId,
                quantity: req.body.quantity
            })

            return order
                .save()

        })
        .then(result => {
            res.status(201).json({
                message: "Order was created",
                order: result,
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/orders/' + result._id
                }
            })
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({
                error: error
            })
        });

});

exports.getOrder = ((req, res, next) => {

    Order.findById(req.params.orderId)
        .populate('productId', 'name price description')
        .exec()
        .then(order => {
            if (!order) {
                res.status(404).json({
                    message: "Order not found"
                })
            }
            res.status(200).json({
                message: "Order details",
                order: order,
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/orders'
                }
            });
        })
        .catch(error => {
            res.status(500).json({ error: error })
        });

});

exports.deleteOrder = ((req, res, next) => {
    Order.remove({ _id: req.params.orderId })
        .then(result => {
            res.status(200).json({
                message: "Order deleted",
                request: {
                    type: "POST",
                    url: 'http://localhost:3000/orders',
                    body: {
                        productId: 'ObjectId',
                        quantity: 'Number',
                    }
                }
            })
        }).catch(error => {
            res.status(500).json({
                error: error
            })
        });
});